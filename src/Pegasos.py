import numpy as np
import scipy.io
import matplotlib.pyplot as plt

class Pegasos:
    def __init__(self, train_path, test_path, C, T, loss):
        self.__dataprocess(train_path, test_path)
        self.n = len(self.train_x)

        self.C = C
        self.T = T
        self.lam = 1/(self.n*self.C)
        self.loss = loss

        self.W = np.random.randn(1899, 1)
        self.b = np.random.randn(1, 1)
        self.choose = np.random.choice(self.n, self.T, replace = True)

        self.__Pegasos()

    def __dataprocess(self, train_path, test_path):
        ''' 数据处理函数'''
        train_set = scipy.io.loadmat(train_path)
        test_set = scipy.io.loadmat(test_path)
        self.train_x = train_set['X']
        self.test_x = test_set['Xtest']
        self.train_y = train_set['y'].astype(np.int16) * 2 - 1
        self.test_y = test_set['ytest'].astype(np.int16) * 2 - 1

    def __Pegasos(self):
        ''' 通过 Pegasos 算法训练模型, st为 y * (wx + b)'''
        cost = []
        for t in range(1, self.T + 1):
            eta = 1 / (self.lam * t)
            st = self.train_y[self.choose[t-1]][0] * self.classifier(self.train_x[self.choose[t-1]])
            self.W, self.b = self.loss.gradDes(self.W, self.b, 
                                            np.swapaxes([self.train_x[self.choose[t-1]]], 0, 1), 
                                            self.train_y[self.choose[t-1]][0], st, 
                                            self.lam, eta)
            cost.append(self.loss.loss(self.W, self.lam, st))

        self.__plot(cost)

    def classifier(self, x):
        '''分类器, 当前 W, b 下模型计算结果'''
        return np.matmul(x, self.W) + self.b

    def classify(self):
        ''' 
        分类: 使用训练后的模型对训练集、测试集分类, 并将正确分类的数量存储在数组 num 中
        使用数组? 因为懒得为两个数据集合起两个名字了
        '''
        num = [0, 0]
        for i in range(len(self.train_y)):
            num[0] += 1 if self.classifier(self.train_x[i]) * self.train_y[i][0] >= 1 else 0
        for i in range(len(self.test_y)):
            num[1] += 1 if self.classifier(self.test_x[i]) * self.test_y[i][0] >= 1 else 0

        self.__print(num)

    def __print(self, num):
        ''' 打印模型在训练集和测试集上的测试结果'''
        print('支持向量机')
        print('C = ', self.C)
        print('T = ', self.T)
        print('训练集acc = {}, {}/{}'.format(num[0]/len(self.train_y), num[0], len(self.train_y)))
        print('测试集acc = {}, {}/{}'.format(num[1]/len(self.test_y), num[1], len(self.test_y)))

    def __plot(self, cost):
        ''' 绘制目标函数梯度下降的图像'''
        t = np.arange(self.T)
        plt.plot(t, cost, 'r')
        plt.title("C = {}, T = {}, loss: {}".format(self.C, self.T, self.loss.__name__))
        plt.legend()
        plt.savefig('../output/{}/{}_{}.png'.format(self.loss.__name__, self.C, self.T))

class hinge:
    @staticmethod
    def gradDes(W, b, x, y, st, lam, eta):
        if st < 1 :
            W -= eta * (lam * W - y * x)
            b += eta * y
        else:
            W -= eta * lam * W
        return W, b

    @staticmethod
    def loss(W, lam, st):
            return np.linalg.det(lam / 2 * np.matmul(np.swapaxes(W,0,1), W) + np.max([0, 1-st]))

class exponential:
    @staticmethod
    def gradDes(W, b, x, y, st, lam, eta):
        if st < -8: st = -8
        exp = np.exp(-st)
        W -= eta * (lam * W - y * x * exp)
        b += eta * y * exp
        return W, b

    @staticmethod
    def loss(W, lam, st):
        return np.linalg.det(lam / 2 * np.matmul(np.swapaxes(W,0,1), W) + np.exp(-st))

class logistic:
    @staticmethod
    def gradDes(W, b, x, y, st, lam, eta):
        exp = np.exp(-st)
        W -= eta * (lam * W - y * x * exp / (1 + exp))
        b += eta * y * exp / (1 + exp)
        return W, b

    @staticmethod
    def loss(W, lam, st):
        return np.linalg.det(lam / 2 * np.matmul(np.swapaxes(W,0,1), W) + np.log(1 + np.exp(-st)))