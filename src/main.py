import os
import datetime
import Pegasos

os.chdir(os.path.abspath(os.path.dirname(__file__)))

train_set = '../data/spamTrain.mat'
test_set = '../data/spamTest.mat'

start = datetime.datetime.now()
SVM = Pegasos.Pegasos(train_set, test_set, 100, 50000, Pegasos.hinge)
SVM.classify()
end = datetime.datetime.now()
print("learning time: ", end-start)